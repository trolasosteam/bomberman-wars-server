package parts;

import java.util.ArrayList;

public class Box {
	private int puntos;
	private ArrayList<Object> contenidos;
	
	//public Box(){}
	
	public Box(){
		contenidos=new ArrayList<Object>();
		puntos=50;
	}
	
	
	public void mete(Object cosa){
		contenidos.add(cosa);
	}
	
	public void saca(Object cosa){
		contenidos.remove(cosa);
	}
	
	public void saca(int posicion){
		contenidos.remove(posicion);
	}
	
	public Object devuelve(int posicion){
		return contenidos.remove(posicion);
	}
	
	public ArrayList<Object> contenidos(){
		return contenidos;
	}
	
	public void vacia(){
		contenidos.clear();
	}
	
	public boolean estaVacio(){
		boolean vacio=true;
		if(contenidos.size()>0){
			vacio = false;
		}else{
			vacio=true;
		}
		return vacio;
	}
	
	public void setPuntos(int punts){
		puntos = punts;
	}
	
	public int getPuntos(){
		return puntos;
	}
}
