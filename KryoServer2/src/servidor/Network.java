package servidor;

import packets.SendPlayer;
import parts.Bomba;
import parts.Box;
import parts.Column;
import parts.Mapa;
import parts.Player;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.EndPoint;
import com.esotericsoftware.kryonet.Server;

public class Network {

	static public void register (EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		
		kryo.register(Mapa.class);
		kryo.register(Object.class);
		kryo.register(Object[].class);
		kryo.register(Object[][].class);
		kryo.register(Player.class);
	//	kryo.register(PacketAddPlayerX.class);
	//	kryo.register(MovePlayerX.class);
		kryo.register(Connection.class);
		kryo.register(Server.class);
		kryo.register(Bomba.class);	
		kryo.register(Box.class);	
		kryo.register(Column.class);	
		kryo.register(Connection[].class);
		kryo.register(SendPlayer.class);

	}
	
}