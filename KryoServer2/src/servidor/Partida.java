package servidor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import parts.Bomba;
import parts.Mapa;
import parts.Player;

public class Partida {
	static Hashtable<Integer, Player> players;
	static Mapa map;
	ArrayList<Bomba>bombas;
	
	
	public Partida(Hashtable<Integer, Player> players,Mapa map){
		Partida.players=players;
		Partida.map=map;
	}
	
	public void metePlayer(int id,Player jugador){
		players.put(id, jugador);
	}
	
	public void borraPlayer(int id,Player jugador){
		players.remove(id);
	}
	
	public void borraPlayer(Player jugador){
		players.remove(jugador);
	}
	

}
