package servidor;

import java.io.IOException;
import java.util.HashMap;

import packets.SendPlayer;
import parts.Bomba;
import parts.Mapa;
import parts.Player;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;


public class Servidor2 extends Listener{
	static String separador="----------------------------------------";
	static Server server;
	static int Port = 6000;
	static Mapa map;
	static Player player;
	static HashMap<Connection, Player> playerList = new HashMap<>();//TODAS LAS CONEXIONES
	static HashMap<Connection, Bomba> bombaList = new HashMap<>();//TODAS LAS CONEXIONES
	//static HashMap<Integer, Player> SeekerList = new HashMap<>();//JUGADORES BUSCANDO PARTIDA
	
	public void Separador(){
		System.out.println(separador);
	}
	
	
	public Servidor2() throws IOException{
		//Creo el servidor
		server = new Server();
		Network.register(server);

		server.bind(Port, Port);
		server.start();
		System.out.println("Server - ON -");
		
		//creo mapa en el servidor
		map = new Mapa();
		//map.creaMapa();
		map.creaMapaObject2();
		
		//pongo el listener del servidor
		server.addListener(this);
		System.out.println("SERVER: escuchando...");
		Separador();
	}
	
	//Cuando se conecta un cliente hace esto
	public void connected(Connection conn){
		System.out.println("SERVER: nuevo cliente conectado desde: "+conn.getRemoteAddressTCP().getAddress());
		//POR CADA NUEVA CONEXIO SE CREA UN PLAYER
		player = new Player();
		//Connection cone = conn;
		//player.connect = cone;
		player.Id=conn.getID();

		if(conn.getID()==1 || conn.getID()%4==1){
			//map.tablero[0][0] = player;
			player.posX = 0;
			player.posY = 0;
			player.networkPosX = 0;
			player.networkPosY = 0;
			player.apodo="Gandalf";
		}else if(conn.getID()==2  || conn.getID()%4==2){
			//map.tablero[12][12] = player;
			player.posX = 12;
			player.posY = 12;
			player.networkPosX = 12;
			player.networkPosY = 12;
			player.apodo="Goku";

		}else if(conn.getID()==3  || conn.getID()%4==3){
			//map.tablero[0][12] = player;
			player.posX = 0;
			player.posY = 12;
			player.networkPosX = 0;
			player.networkPosY = 12;
			player.apodo="Rubius";

		}else if(conn.getID()==4  || conn.getID()%4==0){
			//map.tablero[0][12] = player;
			player.posX = 12;
			player.posY = 0;
			player.networkPosX = 12;
			player.networkPosY = 0;
			player.apodo="Huevo33";

		}
		player.puntos=0;
		player.live=true;
		
		SendPlayer paquete = new SendPlayer("nuevo", player);
		//MANDAMOS EL PLAYER CREADO A LA NUEVA CONEXION
		conn.sendTCP(paquete);
		
		

		//AL NUEVO CLIENTE SE LE A�ADEN TODOS LOS ID DE LOS JUGADORES DE PLAYLIST
		if(playerList.size()>0){
			//RECORREMOS LOS PLAYERS
			for(Player pla : playerList.values()){
				SendPlayer paquete2 = new SendPlayer("nuevo", pla);
				conn.sendTCP(paquete2);
				System.out.println("SERVER: mandando "+pla.apodo+" a "+player.apodo);
			}
		}
		
		//A TODOS LOS JUGADORES DE PLAYLIST SE LES AGREGA EL CLIENTE
		if(playerList.size()>0){
			//RECORREMOS LAS CONEXIONES
			for(Connection conx : playerList.keySet()){
				SendPlayer paquete3 = new SendPlayer("nuevo", player);

				conx.sendTCP(paquete3);
				System.out.println("SERVER: mandando "+player.apodo+" a "+playerList.get(conx).apodo);
			}
		}
		
		//A�ADIMOS EL NUEVO JUGADOR A PLAYERLIST EN EL SERVER
		playerList.put(conn, player);
		System.out.println("SERVER: agregamos jugador "+player.apodo+" con ID: "+conn.getID()+" a la playerlist del servidor");
		
		//FINALMENTE MANDAMOS EL MAPA
		conn.sendTCP(map);
		System.out.println("SERVER: enviado mapa a "+player.apodo);
		Separador();
	}
	
	
	//cuando recibe un paquete de alguna conexion
	public void received(Connection conn, Object ob){
		synchronized (this) {
			if(ob instanceof Mapa){
				System.out.println("SERVER: recibo mapa del Cliente: "+conn.getID());
				map=(Mapa) ob;
				server.sendToAllTCP(map);
				Separador();
	
			}else if(ob instanceof Bomba){
				
				if(!bombaList.containsKey(conn)){
					bombaList.put(conn, (Bomba) ob);
					System.out.println("SERVER: a�adiendo bomba de "+playerList.get(conn).apodo);
				}else{					
					bombaList.remove(conn);
					System.out.println("SERVER: eliminando bomba de "+playerList.get(conn).apodo);
				}
				server.sendToAllTCP((Bomba) ob);
				System.out.println("SERVER: enviando bomba de"+playerList.get(conn).apodo+" a todos");
				Separador();
			}else if(ob instanceof Player){
				Player auxP = (Player) ob;
				if(playerList.containsValue(auxP)){
					System.out.println("SERVER: este player ya existe");
					playerList.put(conn, auxP);
				}
				SendPlayer paquete4 = new SendPlayer("modificar", auxP);
				server.sendToAllTCP(paquete4);
				System.out.println("SERVER: actualizando jugador "+auxP.apodo+" y lo envio a todos");
				Separador();
	
			}else{
				//System.out.println(ob.getClass());
			}
		}
	}
	

	//cuando se desconecta un cliente lo elimino del hash
	public void disconnected(Connection conn){
		System.out.println(playerList.get(conn).apodo+" se ha desconectado");
		//CAMBIAMOS LA VARIABLE Y MANDAMOS EL PLAYER A TODOS LOS CLIENTES
		playerList.get(conn).online=false;
		SendPlayer paquete5 = new SendPlayer("borrar",playerList.get(conn));
		server.sendToAllTCP(paquete5);
		//LO BORRAMOS DEL PLAYLIST DEL SERVIDOR
		playerList.remove(conn);
		System.out.println("jugadores Conectados: "+playerList.size());
		//SIN JUGADORES SE REINICIALIZA EL MAPA
		if(playerList.size()<1){
			map.creaMapaObject2();
		}
		Separador();
	}
	
	//main
	public static void main(String[] args) throws IOException {
		Servidor2 servidor = new Servidor2();
		
	}

}
